#Projeto FreePBX Brasil

```python

Script de Instalação automatizada do FreePBX no CentOS7


```

* Freepbx V.14;
* Asterisk 13;
* CentOS 7.

# Modulos

* timeconditions;
* bulkhandler;
* customcontexts;
* ringgroups;
* queues;
* ivr;
* asteriskinfo;
* iaxsettings.


##Observações:
	
```python

Antes de rodar o script cheque se o seu servidor está com acesso a internet.
```	
